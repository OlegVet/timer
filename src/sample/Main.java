package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Main extends Application {
  private Parent root;
  private Timer timer;
  private final static long period = 10000;
  private final static long workDuration = 9*3600000 + 10*60000; // 9 часов 10 минут в миллисекундах
  private Date startTime;
  private Calendar startCalendar = Calendar.getInstance();
  private Calendar durationCalendar = Calendar.getInstance();
  private Calendar endCalendar = Calendar.getInstance();
  private Format formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
  private Format dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
  private Format timeFormatter = new SimpleDateFormat("HH:mm");
  private String pathFile;

  @Override
  public void start(Stage primaryStage) throws Exception {
    root = FXMLLoader.load(getClass().getResource("sample.fxml"));
    primaryStage.setTitle("Work timer");
    primaryStage.initStyle(StageStyle.UTILITY);
    primaryStage.setOpacity(0);

    try {
      primaryStage.setAlwaysOnTop(true);
    } catch (Exception ignore) {
    }

    Stage secondaryStage = new Stage();
    secondaryStage.initOwner(primaryStage);
    secondaryStage.initStyle(StageStyle.UNDECORATED);
    secondaryStage.setResizable(false);
    secondaryStage.setX(1000);
    secondaryStage.setY(0);
    secondaryStage.setScene(new Scene(root, 170, 25));

    primaryStage.show();
    secondaryStage.show();

    rememberStartTime();
    startTimer();
    update();
  }

  @Override
  public void stop() throws Exception {
    super.stop();
    timer.cancel();
  }

  /**
   * Сохранить в файле время запуска, если сегодня приложение еще не запускалось
   */
  private void rememberStartTime(){
    Date startTimeLocal = Calendar.getInstance().getTime();

    // считать дату запуска из файла
    String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath().replaceFirst("/", "") + "start.ini";
    try {
      pathFile = URLDecoder.decode(path, "UTF-8");
      for (String line : Files.readAllLines(Paths.get(pathFile), Charset.forName("Windows-1251"))) {
        if (line.isEmpty()) continue;
        ParsePosition pp = new ParsePosition(0);
        Date startTimeFromFile = ((SimpleDateFormat) formatter).parse(line, pp);
        System.out.println("сохранено: " + formatter.format(startTimeFromFile));
        if (dateFormatter.format(startTimeFromFile).equals(dateFormatter.format(startTimeLocal))){
          // сегодня приложение уже стартовало
          startTime = startTimeFromFile; // берем сохраненное время
          return;
        }
        // сегодня приложение еще не стартовало
        break;
      }
    } catch (IOException e) {
      // ошибка чтения файла
//      e.printStackTrace();
    }
    // записать текущее время в файл
    try {
      startTime = startTimeLocal;
      Files.write(Paths.get(pathFile), formatter.format(startTime).getBytes(), StandardOpenOption.CREATE);
      System.out.println("запись: " + formatter.format(startTime));
    } catch (IOException e) {
//      e.printStackTrace();
    }

  }

  /**
   * Запустить таймер отображения времени
   */
  private void startTimer(){
    timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        update();
      }
    }, period, period);
  }

  /**
   * Вывести новое значение таймера в окно
   */
  private void update(){
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        Main.this.rememberStartTime();
        // время начала работы
        startCalendar.setTime(startTime);
        // время окончания работы
        endCalendar.setTimeInMillis(startCalendar.getTimeInMillis() + workDuration);
        // прошло времени
//      long remain;
//      durationCalendar.setTimeInMillis(duration = (Calendar.getInstance().getTimeInMillis() - startCalendar.getTimeInMillis() - 4*3600000));
        long remain = endCalendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (remain < 0) remain = 0;
        durationCalendar.setTimeInMillis(remain - 4 * 3600000);

        String s =
            "осталось " + timeFormatter.format(durationCalendar.getTime()) +
                " (выход " + timeFormatter.format(endCalendar.getTime()) + ")";
        Label displayTime = (Label) root.lookup("#displayTime");
        displayTime.setText(s);
        if (Calendar.getInstance().getTime().after(endCalendar.getTime()))
          displayTime.setStyle("-fx-text-fill: #ff0000");
        else
          displayTime.setStyle("-fx-text-fill: #4fcd5f");
      }
    });
  }

  public static void main(String[] args) {
    launch(args);
  }
}
